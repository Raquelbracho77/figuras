.. https://sphinx-themes.org/

Figuras Geométricas
===================

Una **figura geométrica** es la representación visual y funcional de un conjunto no vacío y cerrado de puntos en un plano geométrico. Es decir, figuras que delimitan superficies planas a través de un conjunto de líneas (lados) que unen sus puntos de un modo específico. Dependiendo del orden y número de dichas líneas hablaremos de una figura o de otra.

Clase Figura (base general)
===========================
.. automodule:: figuras.base
   :members:
   :special-members:

Clase Circulo
=============
.. automodule:: figuras.circulo
   :members:
   :special-members:


Clase Triangulo
===============
.. automodule:: figuras.triangulo
   :members:
   :special-members:

Clase Cuadrado
==============
.. automodule:: figuras.cuadrado
   :members:
   :special-members:

Clase Polígono Regular
======================
.. automodule:: figuras.PoligonoRegular
   :members:
   :special-members:

Create index whit sphinx
========================




 
* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

from .base import Figura
import math

class PoligonoRegular(Figura):
	"""
	Clase que permite el calculo de polígonos
	regulares.
	"""
	def __init__(self, a, cant_lados):
		self.a = a # lado
		self.cant_lados = cant_lados

	def __str__(self):
		return f'Polígono Regular = (a = {self.a}, cant_lados = {self.cant_lados})'

	def __repr__(self):
		#print("error")
		return f'Polígono Regular = (a = {self.a}, cant_lados = {self.cant_lados})'


	def to_dict(self):
		diccionario = super().to_dict()
		diccionario.update({'a':self.a , 'cant_lados':self.cant_lados})
		return diccionario


	def area(self):
		"""
		Metódo para calcular:

		 - área del triangulo: 
		 	``a * apotema / 2``

		 - área total del polígono:
		 	``área total = cantidad de lados * área del triangulo``
		"""
		area_tr = self.a * self.apotema / 2
		area_tot = self.cant_lados * area_tr

		return area_tot

	def perimetro(self):
		"""
		Éste método define el producto (*) del número
		de los lados del  multiplicado por el valor de 
		cualquier lado, ya que todos tienen ma misma medida.
		``p = número de lados * a``
		"""
		perimetro = self.cant_lados * self.a
		
		return perimetro

	#print("Error test")
	@property
	def alpha(self):
		"""
		``alpha`` como método, calcula el ángulo del
		triangulo.
		``α =  π / cant_lados``
		"""
		return math.pi / self.cant_lados # alpha = pi / 2

	@property
	def apotema(self): # altura
		"""
		Define la altura total del polígono en cuestión:
			``apotema = a/2 / tan(α)``
		"""
		return (self.a / 2) / math.tan(self.alpha)

class CuadradoEspecial(PoligonoRegular):

	def __init__(self, a):
		super().__init__(a,4)
# /usr/bin/python
from .circulo import Circulo
from .triangulo import Triangulo
from .cuadrado import Cuadrado
from .PoligonoRegular import PoligonoRegular
from abc import ABC, abstractmethod


class Figura(ABC):
	"""
	Declara una abstración
	de una figura geometrica.
	Se define una constante `epsilon` 
	para poder comparar figuras.
	Esta clase debe implementar area
	y perimetro. 

	"""
	epsilon = .0001

	# Circulo
	@abstractmethod
	def area(self):
		pass

	@abstractmethod
	def perimetro(self):
		"""
		Método para definir el perímetro de un circulo
		"""
		pass

	def __eq__(self,figura):
		"""
		Permite comparar *instancias* de figuras
		mediante el valor de *area*, en base al valor
		`epsilon`.
		El método ``__eq__`` compara dos objetos y devuelve 
		True si sus valores son iguales, False de lo contrario.
		"""
		area = self.area()
		return area - self.epsilon <= figura.area() <= area + self.epsilon

	def to_dict(self):
		return {
			'epsilon':self.epsilon, 
			'area':self.area(), 
			'perimetro':self.perimetro(), 
			'nombre':self.__class__.__name__
			}

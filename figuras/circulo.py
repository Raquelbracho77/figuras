from .base import Figura
import math

class Circulo(Figura):
	"""
	Clase que define la figura geometrica de un
	circulo a partir del *area* marcada por su
	*radio*. 
	"""
	def __init__(self, radio):
		"""
		Donde ``__init__`` es el método constructor que creará
		nuevas instancias para la clase **circulo** a partir del
		*parámetro especial* ``self`` que accede a todos los atributos
		y métodos.

		>>> self: Representa las instancias de la clase.
		>>> radio: Segmento que une el centro con cualquier 
		punto de la circunferencia.
		"""
		self.radio = radio

	def __str__(self):
		return f'Circulo(radio={self.radio})'

	def __repr__(self):
		return f'Circulo(radio={self.radio})'


	def to_dict(self):
		diccionario = super().to_dict()
		diccionario.update({'radio':self.radio})
		return diccionario


	def area(self):
		"""
		El área del circulo se define ``A = π * r 2``
		"""
		area = math.pi * math.pow(self.radio, 2)
		return area

	def perimetro(self):
		"""
		Método para definir el perímetro de un circulo,
		según su formula: ``P  = 2 * π r^2``
		"""
		perimetro = 2 * math.pi * self.radio
		return perimetro

from .base import Figura
import math

class Cuadrado(Figura):
	"""
	Clase que define la figura geomética de
	un cuadrado a partir del *perímetro* y 
	*área*.
	"""
	def __init__(self, a):
		self.a = a # lados


	def __str__(self):
		return f'Cuadrado(a={self.a})'

	def __repr__(self):
		return f'Cuadrado(a={self.a})'


	def to_dict(self):
		diccionario = super().to_dict()
		diccionario.update({'a':self.a})
		return diccionario


	def area(self):
		"""
		El cálculo se realiza a traves de la formula
		``A = a^2``
		"""
		area = math.pow(self.a, 2)

		return area

	def perimetro(self):
		"""
		Formula: ``p = 4 * a``
		"""
		perimetro = 4 * self.a

		return perimetro
from .base import Figura
import math

class Triangulo(Figura):
	"""
	Esta clase se encarca de definir la
	figura geométrica para un triangulo
	a partir del perímetro y su area.
	"""

	def __init__(self, a, b, c):
		"""
		Creando campos de instancia y asignando
		el valor del parámetro.
		"""
		self.a = a # lado
		self.b = b # lado izquierdo
		self.c = c # base
		
	def __str__(self):
		return f'Triangulo(a = {self.a}, b = {self.b}, c = {self.c})'

	def __repr__(self):
		return f'Triangulo(a = {self.a}, b = {self.b}, c = {self.c})'


	def to_dict(self):
		diccionario = super().to_dict()
		diccionario.update({'a':self.a, 'b':self.b, 'c':self.c})
		return diccionario


	def area(self):
		"""
		Método que calcula el resultado del área
		de un triangulo a traves de su formula: 
		``area = base*altura/2``
		"""
		sp = self.perimetro() / 2

		area = math.sqrt(sp * (sp -self.a) * (sp - self.b) * (sp - self.c))

		return area

	def perimetro(self):
		"""
		Calcula el perímetro del triangulo a traves de
		su formula: ``P = l + l + l + l``
		"""

		perimetro = self.a + self.b + self.c

		return perimetro

#!/bin/bash

# NORMAL="\e[0m"
# bold="\e[1m"

echo  "\e[33m\e[1mMódulo de testeo para figuras geométricas básicas \e[107\e[0m"
sleep 0.5
echo "\e[1m\e[96mCIRCULO\e[0m"
python tests/test-circulo.py

echo "\e[1m\e[92mCUADRADO\e[0m"
python tests/test-cuadrado.py

echo "\e[1m\e[91mTRIANGULO\e[0m"
python tests/test-triangulo.py

echo "\e[1m\e[38;5;198mPOLÍGONO REGULAR\e[0m"
python tests/test-PoligonoRegular.py

sleep 0.5
echo "\e[5m✨ El test ha sido ejecutado exitosamente.\e[0m"




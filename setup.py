from setuptools import setup, find_packages
from pathlib import Path

path = Path(__file__).resolve().parent
with open(path/'README.md', encoding='utf-8') as f:
    long_description = f.read()

with open(path/'VERSION') as version_file:
    version = version_file.read().strip()

requerimientos = []
with open(path/'requirements.txt') as r:
    requerimientos = list(r.readlines())

setup(name='figuras',
      version=version,
      description='Ejercicio de figuras',
      url='https://figuras.readthedocs.io/en/latest/',
      author='Raquel Bracho',
      author_email='raquelbracho2424@gmail.com',
      license='GPLv3',
      install_requires=requerimientos,      
      package_dir={'figuras': 'figuras'},
      package_data={
          'figuras': ['../tests', '../docs', '../requeriments.txt']},
      packages=find_packages(),
      include_package_data=True,
      long_description=long_description,
      long_description_content_type='text/markdown',
      zip_safe=False)

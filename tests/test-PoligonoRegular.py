from figuras.PoligonoRegular import PoligonoRegular
from rich import print
import time
import math

def test_area_pol():
	a = 5
	cant_lados = 6
	epsilon = .0001
	# math.pi / self.cant_lados
	#alpha = 2 * math.pi / (cant_lados)

	alpha = math.pi / cant_lados

	apotema = (a / 2) / math.tan(alpha)
	area_tr = a * apotema / 2
	area_tot = cant_lados * area_tr
	# print("Error detected AREA",area_tot)

	pr = PoligonoRegular(a, cant_lados)

	# print("Error detected PR", pr.area())
	assert pr.area() - epsilon <= area_tot <= pr.area() + epsilon, 'AREA:: No son iguales'

def test_perimetro_pol():
	a = 5
	cant_lados = 6
	epsilon = .0001
	perimetro = cant_lados * a # fórmula
	
	pr = PoligonoRegular(a, cant_lados)

	assert pr.perimetro() - epsilon <= perimetro <= pr.perimetro() + epsilon, 'PERIMETRO:: No son iguales'

def test_alpha():
	a = 5
	cant_lados = 6
	epsilon = .0001
	alpha = math.pi / cant_lados
	
	pr = PoligonoRegular(a, cant_lados)

	assert  pr.alpha - epsilon <= alpha <= pr.alpha + epsilon, 'AREA:: No son iguales'

def test_apotema():
	a = 5
	cant_lados = 6
	epsilon = .0001
	alpha = math.pi / cant_lados
	apotema = (a / 2) / math.tan(alpha)

	pr = PoligonoRegular(a, cant_lados)

	assert pr.apotema - epsilon <= apotema <= pr.apotema + epsilon, 'apotema es incorrecto'
	

if __name__ == '__main__':
	test_area_pol()
	print("Revisión en cálculo de atributos:")
	time.sleep(0.5)
	print(" ✨","Test de area: OK")
	test_perimetro_pol()
	print(" ✨","Test de perimetro: OK")
	test_alpha()
	print('\u03B1','Test método alpha: OK')
	test_apotema()
	print('Test método apotema: OK')
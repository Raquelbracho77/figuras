from figuras.circulo import Circulo
from rich import print
import time
import math

def test_area():
	radio = 10
	area = math.pi * math.pow(radio, 2)
	epsilon = .0001

	c = Circulo(radio)

	assert c.area() - epsilon <= area <= c.area() + epsilon, 'AREA:: No son iguales'

def test_perimetro():
	radio = 10
	perimetro = 2 * math.pi * radio
	epsilon = .0001

	c = Circulo(radio)
	assert c.perimetro() - epsilon <= perimetro <= c.perimetro() + epsilon, 'PERIMETRO:: No son iguales'

if __name__ == '__main__':
	time.sleep(0.5)
	test_area()
	print("Revisión en cálculo de aributos:")
	time.sleep(0.5)
	print(" ✨","Test de area: OK")
	time.sleep(0.5)
	test_perimetro()
	time.sleep(0.5)
	print(" ✨","Test de perimetro: OK")

	

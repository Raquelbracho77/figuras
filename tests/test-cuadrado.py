from figuras.cuadrado import Cuadrado
from rich import print
import time
import math

def test_area_tr():
	a = 4 # lados
	area = pow(a, 2)

	epsilon = .0001

	cd = Cuadrado(a)

	assert cd.area() - epsilon <= area <= cd.area() + epsilon, 'AREA:: No son iguales'

def test_perimetro_tr():
	a = 4 # lados
	perimetro = 4 * a
	epsilon = .0001

	cd = Cuadrado(a)

	assert cd.perimetro() - epsilon <= perimetro  <= cd.perimetro() + epsilon, 'PERIMETRO:: No son iguales'

if __name__ == '__main__':
	time.sleep(0.5)
	test_area_tr()
	print("Revisión en cálculo de aributos:", ":thumbs_up:")
	time.sleep(0.5)
	print(" ✨","Test de area: OK")
	time.sleep(0.5)
	test_perimetro_tr()
	time.sleep(0.5)
	print(" ✨","Test de perimetro: OK")

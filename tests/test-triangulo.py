from figuras.triangulo import Triangulo
from rich import print
import time
import math

def test_area_tr(): 
	a = 6 # lado
	b = 6 # lado izquierdo
	c = 4 # base

	sp = (a + b + c) / 2	
	area = math.sqrt(sp * (sp -a) * (sp - b) * (sp - c))

	epsilon = .0001

	tr = Triangulo(a , b , c)

	assert tr.area() - epsilon <= area <= tr.area() + epsilon, 'AREA:: No son iguales'


def test_perimetro_tr():
	a = 6 # lado
	b = 6 # lado izquierdo
	c = 4 # base
	perimetro = a + b + c
	epsilon = .0001

	tr = Triangulo(a, b, c)

	assert tr.perimetro() - epsilon <= perimetro <= tr.perimetro() + epsilon, 'PERIMETRO:: No son iguales'


if __name__ == '__main__':
	time.sleep(0.5)
	test_area_tr()
	print("Revisión en cálculo de atributos:")
	time.sleep(0.5)
	print(" ✨","Test de area: OK")
	time.sleep(0.5)
	test_perimetro_tr()
	time.sleep(0.5)
	print(" ✨","Test de perimetro: OK")

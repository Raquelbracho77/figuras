from .base import Figura
import math

class Triangulo(Figura):
	"""
	Clase que define la figura geometrica de un
	*triangulo* a partir del *area* marcada por su
	*radio*. 
	"""
	
	def __init__(self, radio):
		
		self.radio = radio

	def area_triangulo(self):

		area = math.pi * math.pow(self.radio, 2)
		return area

	def perimetro_triangulo(self):
		perimetro = 2 * math.pi * self.radio
		return perimetro